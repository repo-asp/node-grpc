echo "Creating certs folder ..."
mkdir certs && cd certs

echo "Generating certificates ..."

openssl genrsa -passout pass:1078 -aes128 -out ca.key 4096

openssl req -passin pass:1078 -new -x509 -days 365 -key ca.key -out ca.crt -subj  "/C=CL/ST=RM/L=Santiago/O=Test/OU=Test/CN=ca"

openssl genrsa -passout pass:1078 -aes128 -out server.key 4096

openssl req -passin pass:1078 -new -key server.key -out grpc-test.com.csr -subj  "/C=CL/ST=RM/L=Santiago/O=Test/OU=Server/CN=localhost"

openssl x509 -req -passin pass:1078 -days 365 -in grpc-test.com.csr -CA ca.crt -CAkey ca.key -set_serial 01 -out grpc-test.com.crt

openssl rsa -passin pass:1078 -in server.key -out server.key

openssl genrsa -passout pass:1078 -aes128 -out client.key 4096

openssl req -passin pass:1078 -new -key client.key -out client.csr -subj  "/C=CL/ST=RM/L=Santiago/O=Test/OU=Client/CN=localhost"

openssl x509 -passin pass:1078 -req -days 365 -in client.csr -CA ca.crt -CAkey ca.key -set_serial 01 -out client.crt

openssl rsa -passin pass:1078 -in client.key -out client.key